import { LogLevel } from '@nestjs/common';

export interface DiscordLoggerOptions {
   webhookUrl: string;
   level?: LogLevel;
   application?: string;
   environment?: string;
   maxRetries?: number;
   retryDelay?: number;
}

export interface LogMessage {
   level: string;
   message: string;
   timestamp: string;
   [key: string]: any;
}