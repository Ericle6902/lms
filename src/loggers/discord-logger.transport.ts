// import { Transport } from '@elastic/elasticsearch';
import * as Transport from 'winston-transport';
import { WebhookClient } from 'discord.js';
import { DiscordLoggerOptions } from './discord-logger.interface';

export class DiscordTransport extends Transport {
   private webhook: WebhookClient;
   private options: DiscordLoggerOptions;

   constructor(otps: DiscordLoggerOptions) {
      super();
      // this.webhook = new WebhookClient();
   }
}